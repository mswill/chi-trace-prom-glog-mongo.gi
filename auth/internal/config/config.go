package config

import (
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

type config struct {
	DB   string
	Port string
}

func InitConfig() {
	//setDefaultConfig()
	//consulClient, _ := api.NewClient(api.DefaultConfig())
	//kv := consulClient.KV()
	//value, _, err := kv.Get(ServiceName+"/config", nil)
	//if err != nil {
	//	panic(err)
	//}
	//
	//var conf config
	//err = json.Unmarshal(value.Value, &conf)
	//if err != nil {
	//	panic(err)
	//}
	//viper.Set(DB, conf.DB)
	//viper.Set(PORT, conf.Port)
}
func InitConfigRemoteConsul() {
	setDefaultConfig()
	err := viper.AddRemoteProvider("consul", "consul:8500", ServiceName+"/"+"config")
	//err := viper.AddRemoteProvider("consul", "localhost:8500", ServiceName+"/"+"config")
	if err != nil {
		panic(err)
	}
	viper.SetConfigType("json")

	err = viper.ReadRemoteConfig()
	if err != nil {
		panic("Read remote config from Consul: " + err.Error())
	}

}
func setDefaultConfig() {
	viper.AutomaticEnv()
	viper.SetDefault("PORT_AUTH", "11000")
}
