package tracing

import (
	jaeger "github.com/uber/jaeger-client-go"
	jaegerCfg "github.com/uber/jaeger-client-go/config"
)

func InitJaeger() *jaegerCfg.Configuration {
	jaCfg := jaegerCfg.Configuration{
		ServiceName: "mini-client",
		Sampler: &jaegerCfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		},
		Reporter: &jaegerCfg.ReporterConfig{
			LogSpans:          true,
			CollectorEndpoint: "http://jaeger:14268/api/traces",
		},
	}

	return &jaCfg
}

/*


cfg, err := jconfig.FromEnv()
    if err != nil {
        log.Error("cannot parse Jaeger env vars")
    }

    cfg.ServiceName = service
    cfg.Sampler.Type = jaeger.SamplerTypeConst
    cfg.Sampler.Param = 1
    cfg.Reporter.LogSpans = true

*/
