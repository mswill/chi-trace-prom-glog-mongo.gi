package internal

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	AuthRegisteredUser = promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "auth_registered_users_total",
		Help: "Auth registered users total",
	}, []string{"file", "method", "statusCode"})
)
