package internal

import (
	"context"
	"github.com/opentracing/opentracing-go"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Auth struct {
	ID       primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name     string             `json:"name" validate:"required,min=3"`
	Email    string             `json:"email" validate:"required,email"`
	Password string             `json:"password" validate:"required,min=5"`
	Logged   bool               `json:"logged"`
}

type IRepository interface {
	SignUp(ctx context.Context, spanCtx opentracing.SpanContext, auth *Auth) (string, error)
	SignIn(ctx context.Context, spanCtx opentracing.SpanContext, email, password string) (*Auth, error)
	Logout(ctx context.Context, id string) (string, error)

	findUserWithEmail(ctx context.Context, e string) bool
}
