up:
	docker-compose up --build

down:
	docker-compose down

cp:
	docker container prune




# auth/config
# {
#  "DB":"mongodb://mongo:27017",
#  "PORT_AUTH":":11000",
# }

# client/config
# {
#  "DB":"mongodb://mongo:27017",
#  "PORT_CLIENT":":12000"
# }
