package main

import (
	"client/internal"
	"client/internal/config"
	"client/pkg/tracing"
	"context"
	"errors"
	"fmt"
	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/go-chi/chi/v5"
	"github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	//log.SetOutput(os.Stdout)
}

func main() {
	//- Config
	config.InitConfigRemoteConsul()

	//- Start
	log.WithFields(log.Fields{
		"Start Auth service": time.Now(),
	})

	//- Chi Router
	r := chi.NewRouter()

	//- http server
	s := http.Server{
		Addr:    viper.GetString("PORT_CLIENT"),
		Handler: r,
	}

	//- Graylog
	InitGrayLog()

	//- Jaeger
	jCfg := tracing.InitJaeger()

	tracer, err := jCfg.InitGlobalTracer("miniblog-client")
	if err != nil {
		fmt.Println("---- client jaeger err ---- ", err.Error())
		return
	}
	internal.Tr = opentracing.GlobalTracer()
	defer tracer.Close()

	//- Handlers
	handlers := internal.NewHandlers()
	handlers.Public(r)

	//- Start Server
	go start(&s)

	//- Listen OS Signal
	stopCh, closeCh := createChannel()
	defer closeCh()
	log.Println("notified:", <-stopCh)

	//- Server shutdown
	shutdown(context.Background(), &s)

}

func start(server *http.Server) {
	log.WithFields(log.Fields{
		"Server start": viper.GetString("PORT_CLIENT"),
	}).Info("client service start")

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		panic(err)
	} else {
		log.Println("Client: application stopped gracefully")
	}
}
func shutdown(ctx context.Context, server *http.Server) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		panic(err)
	} else {
		log.WithFields(log.Fields{
			"Client Server": "Application Shutdowned",
		}).Info()
	}
}
func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}
func InitGrayLog() {
	hook := graylog.NewGraylogHook("graylog:12201", map[string]interface{}{"service": config.ServiceName})
	//hook := graylog.NewGraylogHook("localhost:12201", map[string]interface{}{"service": config.ServiceName})
	log.AddHook(hook)
}
