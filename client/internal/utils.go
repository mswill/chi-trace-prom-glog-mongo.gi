package internal

import (
	"client/internal/config"
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"strings"
)

func GetReqIDFromContext(c context.Context) string {
	reqId := c.Value(config.ReqID)
	return reqId.(string)
}
func TrimAndToLowerCaseString(str string) string {
	return strings.ToLower(strings.TrimSpace(str))
}

func CheckHash(hash, password string) bool {
	fmt.Println("CheckHash")
	if hash == password {
		return true
	}

	return false
}
func Log(file, method, reqID string) logrus.Fields {
	return logrus.Fields{
		"file":   file,
		"method": method,
		"reqID":  reqID,
		//"time":   time.Now().Local(),
	}
}
