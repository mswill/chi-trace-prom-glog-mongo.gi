package internal

import (
	"auth/internal/config"
	"context"
	"crypto/sha1"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/golang-jwt/jwt/v4"
	"github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	"strings"
	"time"
)

var _ IService = &Service{}

const (
	salt  = "jdhfh4j55"
	logic = " logic:"
)

var signKey = []byte("tokenSecretdjkfhgfdg")

type Service struct {
	repo Repository
}

func NewService(repo Repository) *Service {
	return &Service{repo: repo}
}

// SignUp Done
func (s Service) SignUp(ctx context.Context, spanCtx opentracing.SpanContext, name, email, password string) (string, error) {
	select {
	case <-ctx.Done():
		log.WithFields(log.Fields{"context": "was canceled"}).Info(config.ServiceName+logic, " SignUp: cancel context")
		return "", nil
	default:
		//-
		span := Tr.StartSpan("logic:SignUp", opentracing.ChildOf(spanCtx))
		defer span.Finish()

		//-
		reqID := GetReqIDFromContext(ctx)
		log.WithFields(Log("logic", "SignUp", reqID)).Info(config.ServiceName+logic, " SignUp")
		span.LogKV("req-id", reqID)

		//-
		var a Auth
		v := validator.New()
		name = TrimAndToLowerCaseString(name)
		email = TrimAndToLowerCaseString(email)
		password = strings.TrimSpace(password)

		a.Name = name
		a.Email = email
		a.Password = password

		//-
		err := v.Struct(&a)
		a.Password = GenerateHash(spanCtx, password)
		if err != nil {
			return ErrEmailOrPassword.Error(), err
		}
		up, err := s.repo.SignUp(ctx, span.Context(), &a)

		return up, nil
	}
}

// SignIn Done
func (s Service) SignIn(ctx context.Context, spanCtx opentracing.SpanContext, email, password string) (string, error) {
	select {
	case <-ctx.Done():
		log.WithFields(log.Fields{"context": "was canceled"}).Info(config.ServiceName+logic, " SignIn: cancel context")
		return "", nil
	default:

		//- Trace
		span := Tr.StartSpan(config.ServiceName+"logic:SighIn", opentracing.ChildOf(spanCtx))
		defer span.Finish()

		//- Graylog
		reqID := GetReqIDFromContext(ctx)
		log.WithFields(Log("logic", "SignIn", reqID)).Info(config.ServiceName+logic, "SignIn")
		span.LogKV("req-id", reqID)

		//- logic
		email = TrimAndToLowerCaseString(email)
		password = TrimAndToLowerCaseString(password)

		//-
		var a Auth
		a.Email = email
		a.Password = GenerateHash(span.Context(), password)

		resUser, err := s.repo.SignIn(ctx, span.Context(), a.Email, a.Password)
		if err != nil {
			return ErrNotFoundDocument.Error(), err
		}
		token, _ := s.GenerateToken(ctx, span.Context(), resUser)

		return token, nil
	}
}

func (s Service) Logout(ctx context.Context, spanCtx opentracing.SpanContext, id string) (string, error) {
	//TODO implement me
	panic("implement me")
}

func (s Service) GenerateToken(ctx context.Context, spanCtx opentracing.SpanContext, a *Auth) (string, error) {
	//-
	span := Tr.StartSpan("logic:GenerateToken", opentracing.ChildOf(spanCtx))
	defer span.Finish()

	//-
	t := jwt.New(jwt.SigningMethodHS256)
	claims := t.Claims.(jwt.MapClaims)

	claims["ID"] = a.ID
	claims["name"] = a.Name
	claims["email"] = a.Email
	claims["logged"] = a.Logged
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	token, err := t.SignedString(signKey)
	if err != nil {
		fmt.Println("Token err: ", err)
	}

	return token, nil
}
func GenerateHash(spanCtx opentracing.SpanContext, password string) string {
	span := Tr.StartSpan("logic:GenerateHash", opentracing.ChildOf(spanCtx))
	defer span.Finish()

	//-
	hash := sha1.New()
	hash.Write([]byte(password))
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}

//log.WithFields(log.Fields{
//"file":       "logic",
//"method":     "SignIn",
//"error":      "",
//config.ReqID: reqID,
//}).Info(config.ServiceName, logic, "SignIn")
