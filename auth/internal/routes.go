package internal

import "github.com/go-chi/chi/v5"

func (h *Handlers) Public(r *chi.Mux) {

	r.Post("/signup", h.SignUp)
	r.Post("/signin", h.SignIn)
	r.Post("/logout", h.Logout)

}
