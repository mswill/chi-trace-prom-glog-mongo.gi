package internal

import (
	"auth/internal/config"
	"context"
	"encoding/json"
	"fmt"
	"github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	"net/http"

	"time"
)

type Handlers struct {
	service Service
}

const (
	handlers = " handlers:"
)

var Tr opentracing.Tracer

type InputData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func NewHandlers(service Service) *Handlers {
	return &Handlers{service: service}
}

// SignUp Done
func (h *Handlers) SignUp(w http.ResponseWriter, r *http.Request) {

	//- trace
	spanCtx, _ := Tr.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
	span := opentracing.StartSpan("handlers:SignUp", opentracing.ChildOf(spanCtx))
	fmt.Println("span ---- ", span.Context())
	defer span.Finish()

	//- graylog
	reqID := GetReqIDFromContext(r.Context())
	log.WithFields(Log("handlers", "SignUp", reqID)).Info(config.ServiceName + handlers + r.URL.Path)
	ctx, _ := context.WithTimeout(r.Context(), time.Second*10)
	span.LogKV("req-id", reqID)

	//-
	var a Auth
	err := json.NewDecoder(r.Body).Decode(&a)
	if err != nil {
		return
	}

	//-
	up, err := h.service.SignUp(ctx, span.Context(), a.Name, a.Email, a.Password)
	if up == ErrEmailOrPassword.Error() {
		json.NewEncoder(w).Encode(map[string]interface{}{
			"msg": up,
		})
		return
	}
	if up == ErrUserAlreadyExists.Error() {
		json.NewEncoder(w).Encode(map[string]interface{}{
			"msg": up,
		})
		return
	}
	if err != nil {
		json.NewEncoder(w).Encode(map[string]interface{}{
			"err": err.Error(),
		})
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"msg": up,
	})
}

// SignIn Done
func (h *Handlers) SignIn(w http.ResponseWriter, r *http.Request) {

	//- trace
	spanCtx, err := Tr.Extract(opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(r.Header))
	span := Tr.StartSpan("handlers:SignIn", opentracing.ChildOf(spanCtx))
	defer span.Finish()

	//- graylog
	reqID := GetReqIDFromContext(r.Context())
	log.WithFields(Log("handlers", "SignIn", reqID)).Info(config.ServiceName + handlers + r.URL.Path)
	ctx, _ := context.WithTimeout(r.Context(), time.Second*10)
	span.LogKV("req-id", reqID)

	//-
	var input InputData
	defer r.Body.Close()
	err = json.NewDecoder(r.Body).Decode(&input)
	if err != nil {
		return
	}

	//-
	in, err := h.service.SignIn(ctx, span.Context(), input.Email, input.Password)

	json.NewEncoder(w).Encode(map[string]interface{}{
		"msg":    in,
		"req-id": reqID,
	})

}

func (h *Handlers) Logout(w http.ResponseWriter, r *http.Request) {

}
