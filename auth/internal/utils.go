package internal

import (
	"auth/internal/config"
	"context"
	"fmt"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

func GetReqId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		//-
		reqId := uuid.New().String()
		newCtx := context.WithValue(r.Context(), config.ReqID, reqId)

		next.ServeHTTP(w, r.WithContext(newCtx))
	})
}

func GetReqIDFromContext(c context.Context) string {
	reqId := c.Value(config.ReqID)
	return reqId.(string)
}

func TrimAndToLowerCaseString(str string) string {
	return strings.ToLower(strings.TrimSpace(str))
}

func CheckHash(hash, password string) bool {
	fmt.Println("CheckHash")
	if hash == password {
		return true
	}

	return false
}

func Log(file, method, reqID string) logrus.Fields {
	return logrus.Fields{
		"file":   file,
		"method": method,
		"reqID":  reqID,
		//"time":   time.Now().Local(),
	}
}
