package internal

import (
	"context"
	"github.com/opentracing/opentracing-go"
)

type IService interface {
	SignUp(ctx context.Context, spanCtx opentracing.SpanContext, name, email, password string) (string, error)
	SignIn(ctx context.Context, spanCtx opentracing.SpanContext, email, password string) (string, error)
	Logout(ctx context.Context, spanCtx opentracing.SpanContext, id string) (string, error)
	GenerateToken(ctx context.Context, spanCtx opentracing.SpanContext, a *Auth) (string, error)
}
