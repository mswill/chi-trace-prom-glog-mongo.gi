package config

import (
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

func InitConfigRemoteConsul() {
	setDefaultConfig()
	err := viper.AddRemoteProvider("consul", "consul:8500", ServiceName+"/"+"config")
	//err := viper.AddRemoteProvider("consul", "localhost:8500", ServiceName+"/"+"config")
	if err != nil {
		panic(err)
	}
	viper.SetConfigType("json")

	err = viper.ReadRemoteConfig()
	if err != nil {
		panic("Read remote config from Consul: " + err.Error())
	}

}

func setDefaultConfig() {
	viper.AutomaticEnv()
	viper.SetDefault("PORT_CLIENT", "12000")
}
