package internal

import "context"

type IService interface {
	SignUp(ctx context.Context, name, email, password string) (string, error)
	SignIn(ctx context.Context, email, password string) (string, error)
	Logout(ctx context.Context, id string) (string, error)
}
