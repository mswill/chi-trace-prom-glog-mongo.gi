package internal

import (
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type ClientAuth struct {
	ID       primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name     string             `json:"name" validate:"required,min=3"`
	Email    string             `json:"email" validate:"required,email"`
	Password string             `json:"password" validate:"required,min=5"`
	Logged   bool               `json:"logged"`
}
