package internal

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	"net/http"
	"strconv"
)

type MetricsMiddleware struct {
	Processed *prometheus.CounterVec
}

func NewMetricsMiddleware() *MetricsMiddleware {
	Processed := promauto.NewCounterVec(prometheus.CounterOpts{
		Name: "auth__service_request_total",
		Help: "The total Auth service requests",
	}, []string{"method", "handler", "statusCode"})
	return &MetricsMiddleware{Processed: Processed}
}

func (m *MetricsMiddleware) AuthTotalMetrics(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		next.ServeHTTP(w, r)

		m.Processed.With(prometheus.Labels{"method": r.Method, "handler": r.URL.Path, "statusCode": strconv.Itoa(http.StatusOK)}).Inc()
	})
}
