package logger

import (
	"github.com/sirupsen/logrus"
)

type log struct {
	logger *logrus.Logger
}

func NewLogger() *log {
	return &log{
		logger: logrus.New(),
	}
}

func (i *log) GetRequest(reqId string) logrus.Fields {
	return logrus.Fields{"req-id": reqId}
}
