package internal

import (
	"client/internal/config"
	"context"
	"github.com/google/uuid"
	"net/http"
)

func GetReqId(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		reqId := uuid.New().String()
		newCtx := context.WithValue(r.Context(), config.ReqID, reqId)

		next.ServeHTTP(w, r.WithContext(newCtx))
	})
}
