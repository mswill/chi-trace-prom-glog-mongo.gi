package internal

import (
	"auth/internal/config"
	"context"
	"errors"
	"github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

var _ IRepository = &Repository{}
var (
	ErrNotFoundDocument  = errors.New("Email or Password is not correct ")
	ErrUserAlreadyExists = errors.New("Email already exists. Please take a new email ")
	ErrEmailOrPassword   = errors.New("Email or Password is invalid ")
)

const (
	repo    = " repository:"
	colName = "users"
)

type Repository struct {
	Db *mongo.Database
}

func NewRepository(db *mongo.Database) *Repository {
	return &Repository{Db: db}
}

// SignUp Done
func (r Repository) SignUp(ctx context.Context, spanCtx opentracing.SpanContext, a *Auth) (string, error) {
	//-
	reqID := GetReqIDFromContext(ctx)
	filter := bson.M{
		"name":     a.Name,
		"email":    a.Email,
		"password": a.Password,
	}
	select {
	case <-ctx.Done():
		log.WithFields(log.Fields{"context": "was canceled"}).Info(config.ServiceName+repo, " SignUp: cancel context")
		return "", nil
	default:
		//- trace
		span := Tr.StartSpan("repo:SignUp", opentracing.ChildOf(spanCtx))
		defer span.Finish()
		span.LogKV("req-id", reqID)

		//- graylog
		if r.findUserWithEmail(ctx, a.Email) {
			log.WithFields(Log("repository", "SignUp", reqID)).Info(config.ServiceName+repo, " SignUp:Err")
			return ErrUserAlreadyExists.Error(), nil
		}

		//- logic
		one, err := r.Db.Collection(colName).InsertOne(ctx, filter)
		if err != nil {
			//- metric
			AuthRegisteredUser.With(prometheus.Labels{"file": "repository", "method": "SignUp", "statusCode": "500"}).Inc()
			return "", err
		}

		//- metric
		AuthRegisteredUser.With(prometheus.Labels{"file": "repository", "method": "SignUp", "statusCode": "200"}).Inc()

		//-
		log.WithFields(log.Fields{
			"file":               "repository",
			"method":             "SignUp",
			"registered_user_id": one.InsertedID,
			config.ReqID:         reqID,
		}).Info(config.ServiceName, repo, "SignUp")
		return "register success", nil
	}
}

// SignIn Done
func (r Repository) SignIn(ctx context.Context, spanCtx opentracing.SpanContext, email, password string) (*Auth, error) {
	//-
	reqID := GetReqIDFromContext(ctx)
	filter := bson.M{
		"email":    email,
		"password": password,
	}
	var a Auth
	select {
	case <-ctx.Done():
		log.WithFields(log.Fields{"context": "was canceled"}).Info(config.ServiceName+repo, " SignIn: cancel context")
	default:
		//- trace
		span := Tr.StartSpan("repo:SignIn", opentracing.ChildOf(spanCtx))
		span.LogKV("req-id", reqID)
		defer span.Finish()

		//- graylog
		log.WithFields(Log("repository", "SignIn", reqID)).Info(config.ServiceName+repo, "SignIn")

		//- logic
		err := r.Db.Collection(colName).FindOne(ctx, filter).Decode(&a)

		if err == mongo.ErrNoDocuments {
			return &Auth{}, ErrNotFoundDocument
		}
		if err != nil {
			return &Auth{}, err
		}

		return &a, nil
	}

	return &a, nil
}

func (r Repository) Logout(ctx context.Context, id string) (string, error) {
	//TODO implement me
	panic("implement me")
}

func (r Repository) findUserWithEmail(ctx context.Context, e string) bool {

	filter := bson.M{"email": e}
	var a Auth
	_ = r.Db.Collection("users").FindOne(ctx, filter).Decode(&a)
	if e == a.Email {
		return true
	}
	return false
}
