package main

import (
	"auth/internal"
	"auth/internal/config"
	"auth/pkg/mongo"
	"auth/pkg/tracing"
	"context"
	"errors"
	"fmt"
	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/go-chi/chi/v5"
	"github.com/opentracing/opentracing-go"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	log.SetFormatter(&log.JSONFormatter{})
	//log.SetOutput(os.Stdout)
}

func main() {
	//- Config
	config.InitConfigRemoteConsul()

	//- Start
	log.WithFields(log.Fields{
		"Start Auth service": time.Now(),
	})

	//- Chi Router
	r := chi.NewRouter()

	//- http server
	s := http.Server{
		Addr:    viper.GetString("PORT_AUTH"),
		Handler: r,
	}

	//- Graylog
	InitGrayLog()

	//- Mongo
	client := mongo.InitMongo(viper.GetString("DB"))
	dbName := client.Database("miniblog")

	//- Jaeger
	jCfg := tracing.InitJaeger()
	tracer, err := jCfg.InitGlobalTracer("miniblog-auth")
	if err != nil {
		fmt.Println("--- jaeger --- ", err.Error())
		return
	}
	internal.Tr = opentracing.GlobalTracer()
	defer tracer.Close()

	//- Dependency injection
	repo := internal.NewRepository(dbName)
	srv := internal.NewService(*repo)
	handlers := internal.NewHandlers(*srv)

	// middleware
	metrics := internal.NewMetricsMiddleware()
	r.Use(internal.GetReqId)
	r.Use(metrics.AuthTotalMetrics)

	//- Prometheus metrics
	r.Handle("/metrics", promhttp.Handler())

	//- Routers
	handlers.Public(r)

	//- Start Server
	go start(&s)

	//- Listen OS Signal
	stopCh, closeCh := createChannel()
	defer closeCh()
	log.Println("notified:", <-stopCh)

	//- Server shutdown
	shutdown(context.Background(), &s)

}
func start(server *http.Server) {
	log.WithFields(log.Fields{
		"Server start": viper.GetString("PORT_AUTH"),
		"Mongo start":  viper.GetString("DB"),
	}).Info("auth service start")

	if err := server.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
		panic(err)
	} else {
		log.Println("Auth: application stopped gracefully")
	}
}
func shutdown(ctx context.Context, server *http.Server) {
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		panic(err)
	} else {
		log.WithFields(log.Fields{
			"Auth Server": "Application Shutdowned",
		}).Info()
	}
}
func createChannel() (chan os.Signal, func()) {
	stopCh := make(chan os.Signal, 1)
	signal.Notify(stopCh, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	return stopCh, func() {
		close(stopCh)
	}
}
func InitGrayLog() {
	hook := graylog.NewGraylogHook("graylog:12201", map[string]interface{}{"service": config.ServiceName})
	//hook := graylog.NewGraylogHook("localhost:12201", map[string]interface{}{"service": config.ServiceName})
	log.AddHook(hook)
}
