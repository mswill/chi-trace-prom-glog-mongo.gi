package internal

import (
	"client/internal/config"
	"encoding/json"
	"fmt"
	"github.com/opentracing/opentracing-go"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strings"
)

type Handlers struct {
}

func NewHandlers() *Handlers {
	return &Handlers{}
}

const (
	handlers = " handlers:"
)

var Tr opentracing.Tracer

type InputData struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// SignUp Done
func (h *Handlers) SignUp(w http.ResponseWriter, r *http.Request) {
	//- trace
	span := Tr.StartSpan("handlers:SignUp")
	fmt.Println("span ---- ", span.Context())
	defer span.Finish()

	//- graylog
	reqId := GetReqIDFromContext(r.Context())
	log.WithFields(Log("handler", "signup", reqId)).Info(config.ServiceName + handlers + r.URL.Path)

	//-
	var a ClientAuth
	err := json.NewDecoder(r.Body).Decode(&a)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//-
	marshal, err := json.Marshal(a)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//-
	cl := http.Client{}
	req, _ := http.NewRequest("POST", "http://auth:11000/signup", strings.NewReader(string(marshal)))
	//req, _ := http.NewRequest("POST", "http://localhost:11000/signup", strings.NewReader(string(marshal)))

	//-
	_ = Tr.Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))
	res, _ := cl.Do(req)

	//-
	var resData interface{}
	err = json.NewDecoder(res.Body).Decode(&resData)
	if err != nil {
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"localhost:12000": r.URL.Host + " " + r.URL.Path,
		"response":        res.StatusCode,
		"data":            resData,
	})
}

// SignIn Done
func (h *Handlers) SignIn(w http.ResponseWriter, r *http.Request) {
	//- trace
	span := Tr.StartSpan("handlers:SignIn")
	defer span.Finish()

	//- graylog
	reqId := GetReqIDFromContext(r.Context())
	log.WithFields(Log("handler", "signin", reqId)).Info(config.ServiceName + handlers + r.URL.Path)

	//-
	var a InputData
	err := json.NewDecoder(r.Body).Decode(&a)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//-
	marshal, err := json.Marshal(a)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	//-
	cl := http.Client{}
	req, _ := http.NewRequest("POST", "http://auth:11000/signin", strings.NewReader(string(marshal)))
	//req, _ := http.NewRequest("POST", "http://localhost:11000/signin", strings.NewReader(string(marshal)))

	//-
	_ = Tr.Inject(span.Context(), opentracing.HTTPHeaders, opentracing.HTTPHeadersCarrier(req.Header))
	res, _ := cl.Do(req)

	//-
	var resData interface{}
	err = json.NewDecoder(res.Body).Decode(&resData)
	if err != nil {
		return
	}

	_ = json.NewEncoder(w).Encode(map[string]interface{}{
		"localhost:12000": r.URL.Host + " " + r.URL.Path,
		"response":        res.StatusCode,
		"data":            resData,
	})
}

func (h *Handlers) Logout(w http.ResponseWriter, r *http.Request) {

}
